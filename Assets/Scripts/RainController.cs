﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

public class RainController : MonoBehaviour {


    public ParticleSystem rainParticles;
    public GameObject rainSoundObj;

    AudioSource rainAudio;

    bool rainIsOn = false;


    // Use this for initialization
    void Start () {
        rainAudio = rainSoundObj.GetComponent<AudioSource>();
    }
    
    // Update is called once per frame
    void Update () {
        bool togglePressed = CrossPlatformInputManager.GetButtonDown("Rain Switch");

        if (togglePressed)
        {
            if (rainIsOn) 
            {
                TurnOffRain();
            }
            else
            {
                TurnOnRain();
            }
        }
    }

    void TurnOnRain()
    {
        rainAudio.Play();
        rainParticles.Play();
        rainIsOn = true;
    }

    void TurnOffRain()
    {
        rainParticles.Stop();
        rainAudio.Stop();
        rainIsOn = false;
    }
}
