﻿using System.Collections;
using System.Collections.Generic;
using UnityStandardAssets.CrossPlatformInput;
using UnityEngine;

public class DayNightController : MonoBehaviour {

    public Material daySkybox;
    public Material nightSkybox;

    public Color nightFogColor;
    Color dayFogColor;

    public Light sunLight;

    bool itIsDay = true;
    float nightGloomIntensity = -1.2f;
    float dayGloomIntensity = 1.2f;

    // Use this for initialization
    void Start () {
        dayFogColor = RenderSettings.fogColor;

    }
	
	// Update is called once per frame
	void Update () {

        bool togglePressed = CrossPlatformInputManager.GetButtonDown("Night/Day Switch");

        if (togglePressed)
        //if (Input.GetKeyDown(KeyCode.N))
        {
            if (itIsDay)
            {
                TurnOnNight();
            }
            else
            {
                TurnOnDay();
            }
        }
    }

    void TurnOnNight() 
    {
        SetAmbientLightIntensity(nightGloomIntensity);

        RenderSettings.skybox = nightSkybox;
        RenderSettings.fogColor = nightFogColor;

        sunLight.enabled = false;

        itIsDay = false;
    }


    void TurnOnDay()
    {
        SetAmbientLightIntensity(dayGloomIntensity);

        RenderSettings.skybox = daySkybox;
        RenderSettings.fogColor = dayFogColor;

        sunLight.enabled = true;

        itIsDay = true;
    }

    void SetAmbientLightIntensity(float intensity)
    {
        Color ambientColor = RenderSettings.ambientEquatorColor;

        float intensityMul = Mathf.Pow(2.0f, intensity);

        ambientColor.r *= intensityMul;
        ambientColor.g *= intensityMul;
        ambientColor.b *= intensityMul;

        RenderSettings.ambientEquatorColor = ambientColor;
    }
}
