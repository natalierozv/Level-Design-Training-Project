﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThunderClap : MonoBehaviour {

    public AudioClip clapSound;

    bool canFlicker = false;

    Light thunderLight = null;
    AudioSource thunderAudio = null;

    // Use this for initialization
	void Start () {
        thunderLight = GetComponent<Light>();
        thunderAudio = GetComponent<AudioSource>();
    }

    private void OnEnable()
    {
        StartCoroutine(KickOffLightning());
    }

    private void OnDisable()
    {
        StopCoroutine(KickOffLightning());
        StopCoroutine(Flicker());
        canFlicker = false;
    }

    void Update () {
        if (canFlicker) 
        {
            StartCoroutine(Flicker());
        }
	}

    IEnumerator KickOffLightning()
    {
        yield return new WaitForSeconds(3f);
        canFlicker = true;
    }

    IEnumerator Flicker()
    {
        if (canFlicker)
        {
            canFlicker = false;

            thunderAudio.PlayOneShot(clapSound);

            thunderLight.enabled = true;
            yield return new WaitForSeconds(Random.Range(0.6f, 1.0f));
            thunderLight.enabled = false;

            yield return new WaitForSeconds(Random.Range(0.5f, 8f));
            canFlicker = true;
        }
    }
}
