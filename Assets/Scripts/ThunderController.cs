﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

public class ThunderController : MonoBehaviour {

    bool thunderIsOn = false;
    public ThunderClap thunderScript;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        bool togglePressed = CrossPlatformInputManager.GetButtonDown("Thunderstorm Switch");

        if (togglePressed)
        {
            if (thunderIsOn)
            {
                TurnOffThunder();
            }
            else
            {
                TurnOnThunder();
            }
        }
    }

    void TurnOffThunder()
    {
        thunderScript.enabled = false;
        thunderIsOn = false;
    }

    void TurnOnThunder()
    {
        thunderScript.enabled = true;
        thunderIsOn = true;
    }
}
